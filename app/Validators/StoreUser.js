'use strict'

class StoreUser {
  get rules () {
    return {
      email: 'required|email|unique:users',
      password: 'required',
      username: 'required|unique:users',
      gender: 'required',
    }
  }
}

module.exports = StoreUser
