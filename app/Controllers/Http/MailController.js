'use strict';

const Mail = use('Mail')

class MailController {

  /*
  * Send email to admin with comment requiring more info
  *
  */
  async moreInfo ({request}) {
    const BODY = request.post();
    console.log(BODY)
    await Mail.send('emails.more-info', BODY, (message) => {
      message
        .to('infobrainamic@gmail.com')
        .from('more-info@brainamic.com')
        .subject('Nueva solicitud de información')
    })

  }

}

module.exports = MailController;
