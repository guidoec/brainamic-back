'use strict';

const User = use('App/Models/User');

class UsersController {

  async index() {
    return await User.all()
  }

  async store({request, response}) {
    response.implicitEnd = false;
    const USER = new User();
    const BODY = request.post();

    USER.username = BODY.username;
    USER.email = BODY.email;
    USER.name = BODY.name;
    USER.surname = BODY.surname;
    USER.gender = BODY.gender;
    USER.password = BODY.password;

    try {
      await USER.save();
    } catch (e) {
      response.status(500).send({message: e.sqlMessage, code: e.code})
    }

    response.status(200).send({message: 'User saved succesfully'});
  }

  async show() {
  }


  async update() {
  }

  async destroy() {
  }

  async login({request, response, auth}) {
    const {email, password} = request.all();
    let token = await auth.attempt(email, password);

    let user = await User.query()
                        .where('email', email)
                        .select('email','name','surname','gender','username')
                        .with('services')
                        .first();

    const PAYLOAD = {...user.toJSON(), ...token};

    response.status(200).send(PAYLOAD)
  }

  async logout({response, auth}) {
    try {
      await auth.logout()
    } catch (e) {
      response.status(500).send(e)
    }

    response.status(200).send('User logged out')
  }
}

module.exports = UsersController;
