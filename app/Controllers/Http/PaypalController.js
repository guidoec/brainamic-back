'use strict';
const PaypalRepository = require('../../Repository/PaypalRepository');

class PaypalController {

  constructor() {
    this.paypal = new PaypalRepository();
  }

  /**
   * List all active billing plans
   * @param request
   * @param response
   * @returns {Promise.<void>}
   */
  async plans({response}) {
    let billingPlanConfig = {
      status: 'ACTIVE'
    };

    let planList = await this.paypal.requestBillingPlans(billingPlanConfig);

    response.status(200).send(planList)
  }

  /**
   * Request plan details by id to API
   * @param params
   * @param response
   * @returns {Promise.<void>}
   */
  async planDetail({params, response}) {
    let planDetail = await this.paypal.requestBillingPlanDetail(params.id);

    response.status(200).send(planDetail)
  }

  /**
   * Create plan agreement from API
   * @param request
   * @param response
   * @returns {Promise.<void>}
   */
  async createAgreement({request, response}) {
    let planAgreement = await this.paypal.createPlanAgreement(request.body.planId)

    response.status(200).send(planAgreement)
  }
}

module.exports = PaypalController;
