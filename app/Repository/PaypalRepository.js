'user strict'
const paypal = require('paypal-rest-sdk');

class PaypalRepository {

  constructor() {
    paypal.configure({
      'mode': 'sandbox', //sandbox or live
      'client_id': 'ATzOqdF_b3BDxL5FzBL5z5awYJj8qQBT9XhME-CVe8hdQXBkJuJXP6CzDtROCW2zUQMCxa6uObMqggyB',
      'client_secret': 'EA0ZgdmMauUX7zrl0cK4AAIVn5BUXHhdaI2-2GGj2nQellhsggpZw7zETNbxh-M1ca_FwUOYeX2p6HO_'
    });
  }

  /**
   * Request a list of all billing
   * plans ('ACTIVE', 'INACTIVE' or 'CREATED')
   * @param listConfig
   * @returns {Promise}
   */
  requestBillingPlans(listConfig) {
    return new Promise((resolve, reject) => {
      paypal.billingPlan.list(listConfig, function (error, billingPlan) {
        if (error) {
          reject(error);
        } else {
          resolve(JSON.stringify(billingPlan))
        }
      })
    })
  }

  /**
   * Request billing plan details
   * by id
   * @param planId
   * @returns {Promise}
   */
  requestBillingPlanDetail(planId) {
    return new Promise((resolve, reject) => {
      paypal.billingPlan.get(planId, function (error, billingPlan) {
        if (error) {
          reject(error)
        } else {
          resolve(JSON.stringify(billingPlan))
        }
      });
    })
  }

  /**
   * Create plan agreement
   * @param planId
   */
  createPlanAgreement(planId) {
    const billingAgreementAttributes = {
      "name": "Fast Speed Agreement",
      "description": "Agreement for Fast Speed Plan",
      "start_date": "2019-12-22T09:13:49Z",
      "plan": {
        "id": `${planId}`
      },
      "payer": {
        "payment_method": "paypal"
      }
    };

    return new Promise((resolve, reject) => {
      paypal.billingAgreement.create(billingAgreementAttributes, function (error, billingAgreement) {
        if (error) {
          reject(error)
        } else {
          resolve(billingAgreement)
        }
      });
    })
  }
}

module.exports = PaypalRepository
