'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')

Route.on('/').render('welcome')

/*
 |--------------------------------------------------------------------------
 | Public Routes
 |--------------------------------------------------------------------------
*/
// Create user & login routes
Route
  .group(() => {
    Route.post('users', 'UsersController.store').validator('StoreUser')
    Route.post('login', 'UsersController.login')
  })
  .prefix('api')



Route
  .group(() => {
    Route.post('contact', 'MailController.moreInfo')
  })
  .prefix('api/mail')

/*
 |--------------------------------------------------------------------------
 | Private Routes
 |--------------------------------------------------------------------------
*/

// User routes
Route
  .group(() => {
    Route
      .resource('users', 'UsersController')
      .except(['store'])
      .apiOnly()
    Route.get('logout', 'UsersController.logout')
  })
  .prefix('api')
  .middleware(['auth'])

// Paypal routes
Route
  .group(() => {
    Route.get('plans', 'PaypalController.plans'),
    Route.get('plans/:id', 'PaypalController.planDetail')
    Route.post('plans/agreement', 'PaypalController.createAgreement')
  })
  .prefix('api/paypal')
  .middleware(['auth'])
