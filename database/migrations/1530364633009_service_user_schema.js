'use strict'

const Schema = use('Schema')

class ServiceUserSchema extends Schema {
  up () {
    this.create('service_users', (table) => {
      table.increments()
      table.integer('user_id')
      table.integer('service_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('service_users')
  }
}

module.exports = ServiceUserSchema
