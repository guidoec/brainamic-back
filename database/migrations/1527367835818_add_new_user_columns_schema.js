'use strict'

const Schema = use('Schema')

class AddNewUserColumnsSchema extends Schema {
  up () {
    this.alter('users', (table) => {
      table.integer('gender').after('password')
      table.string('surname', 80).after('password')
      table.string('name', 80).after('password')
    })
  }

  down () {
    this.alter('users', (table) => {
      table.dropColumn('name')
      table.dropColumn('surname')
      table.dropColumn('gender')
    })
  }
}

module.exports = AddNewUserColumnsSchema
